# Micro Fiction Game Jam Tools
A small collection of tools that I have created to maintain the Micro Fiction Game Jam website.

# scripts

## mfheader.fish

This script writes the header part of a page for the website to a file. This is the bit with the <head> tags in it. It also opens the <body> tag ready for the content to be appended after it.

## mffooter.fish
This script closes the <body> tags and writes any needed footer material that should be inserted after the page content. 

## mfmakeweb.fish

This script is used to make the whole website. It will create the index page the submit paged and navigate into the years of the jam and build the entries too.

### Arguments
```
-s Builds the submit page, missing this flag will remove the submit pagefor the site!
```

## mfentries.fish

Create the index page and the individual entriy pages for a jam that has been run.

### use
To create the entries for a given jam cd to the directory the jam livesin and pass the entries file to the script.

```
make_entries entries_file.txt
```

If you do not pass the file name to the function it will assume entries.txt and look for it in the directory.

### Directory structure of a jam
Each year the jam runs should be kept in it's own directory. This should ccontain only two files:

* entries.txt
* blurb.txt

### blurb.txt
when the index page for a years jam is created blurb.txt contains what should be printed before the links to the games. This is normally just the theme that the game ran on that year.
This file must exist as it is looked for by the script automagically.

### entries.txt
Entries are kept in a text file format with the following format:

```
NAME: Person name
WEB: link_to_website
TITLE: Titleofthe game
Anything else on a
non tagged

line is part of the game
text.
%% <- This is what divides entries!
NAME: Another person
WEB: another_link
TITLE: another title
and then some more game text
and so on..
%%
```

Each entry has a few lines that can tag information about the entry, everything else is considdered part fo the game text.  Each entry is divided by a line that starts "%%".
