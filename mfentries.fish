
# make the entries in a given directory.
#
# file - the file that is holding all of the game data.
function mfentries -a file

    # Set the variables that will be used to create a game entry
    set -l name ""
    set -l title ""
    set -l web ""
    set -l game ""

    if test -z $file
        set file entries.txt
    end

    # Create the entries index file
    mfheader index.html
    while read -l line
        echo $line >> index.html
    end < blurb.txt

    set yearly 0 
    # Parse each entry for the 
    while read -l line
        switch $line
            case "%%"
                create_entry $name $web $title $game
                set name ""
                set title ""
                set web ""
                set game ""
		set yearly (math $yearly + 1)
            case "NAME::*"
                set name_data (string split "::" $line)
                set name $name_data[2]
            case "TITLE::*"
                set title_data (string split "::" $line)
                set title $title_data[2]
            case "WEB::*"
                set web_data (string split "::" $line)
                set web $web_data[2]
            case "*"
                set game "$game\n$line"
        end
    end < $file

    echo "<p><a href=\"http://microfictiongames.neocities.org\">home</a> | <a href=\"https://dice.camp/@mfgj\">social</a> | <a href=\"https://sr.ht/~jameschip/micro_fiction_game_jam/\">source</a> </p>" >> index.html
    mffooter index.html
    echo -e "\e[0;33m---> This year the jam had $yearly games!\e[0;0m"
    echo ""
end

# create the page for an entry
#
# nm - the name of the person who submitted it
# we - the web link to the oerson who submitted it
# ti - the title of thegame
# gm - the game text
function create_entry -a nm we ti gm

    set fn (string replace -a ' ' _ (string trim $ti))
    set fn (string replace -r -a '[:&$%$£\\\/\^\*\"\'\!]' '' $fn)
    set fn "$fn.html"
    set fn (string lower $fn)
    if test -e $fn
        echo -e " - \e[0;34m$fn\e[0;0m"
    else
        echo -e " + \e[0;32m$fn\e[0;0m"
    end
    
    mfheader $fn
    echo "<h1>$ti</h1>" >> $fn
    echo "<p>By: <a href=\"$we\">$nm</a> </p>" >> $fn
    echo -e "<pre>$gm</pre>" >> $fn
    echo "<p><a href=\"index.html\">back</a> | <a href=\"http://microfictiongames.neocities.org\">home</a> | <a href=\"https://dice.camp/@mfgj\">social</a> | <a href=\"https://sr.ht/~jameschip/micro_fiction_game_jam/\">source</a></p>" >> $fn
    mffooter $fn
    echo "<a href=\"$fn\">$ti</a><br>" >> index.html
    set -g count (math $count + 1)
    
end

