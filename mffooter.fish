

# Create the footerfor the micro fiction games jam pages
#
# fn - the file name to write to
function mffooter -a fn

	echo "</div>" >> $fn
	echo "<footer>" >> $fn
	echo "<div class=\"content\">" >> $fn
	echo "<p>Hosted on <a href=\"http://neocities.org\">neocities.</a>" >> $fn
	echo "<br>Site designed and run by <a href=\"https://jameschip.io\">James Chip.</a>" >> $fn
	echo "</div>" >> $fn
	echo "</footer>" >> $fn
	echo "</body>" >> $fn
	echo "</html>" >> $fn

end
