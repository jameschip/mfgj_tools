
# Create the header matter for all of the pages
#
# fn - the file name to write to
function mfheader -a fn

	echo "<!DOCTYPE html>" > $fn
	echo "<html>" >> $fn
	echo  "<head>" >> $fn
	echo "<meta charset=\"UTF-8\">" >> $fn
	echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" >> $fn
	echo "<title>Micro Fiction Games</title>" >> $fn
	echo "<link href=\"/styles.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">" >> $fn
	echo "</head>" >> $fn
	echo "<body>" >> $fn
	echo "<div class=\"content\">" >> $fn

end

