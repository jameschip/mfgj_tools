
function mfweb

    set -g count 0

    argparse 's/submit' -- $argv

    # Remove the index and submit pages as they will be replaced.
    rm -f index.html
    
    if ! test -e index.txt

        echo -e "The index file for the website does not exist!"
        echo -e "Build stopped!"
        return

    end

    make_index


    if ! test -z $_flag_s

        make_submit submit.txt
        
    else

        make_submit no_submit.txt
        
    end


    # Make the 2020 entries
    echo "Creating 2020 entries *************************************"
    cd 2020
    mfentries
    cd ..

    # Make the 2021 entries
    echo "Creating 2021 entries *************************************"
    cd 2021
    mfentries
    cd ..

    # Make the 2022 entries
    echo "Creating 2022 entries *************************************"
    cd 2022
    mfentries
    cd ..
    
    # Make the 2023 entries
    echo "Creating 2023 entries *************************************"
    cd 2023
    mfentries
    cd ..
    
	# Make the 2024 entries
    echo "Creating 2024 entries *************************************"
    cd 2024
    mfentries
    cd ..
    echo -e "\e[0;35m →  $count entries in the jam to date! ← \e[0;0m"
    
end

# Create the main index.html
#
function make_index

    echo "Creating main site index.html"
    touch index.html
    mfheader index.html
    
    while read -l line
        echo $line >> index.html
    end < index.txt

    mffooter index.html
    
end

# Create the submit page
#
function make_submit -a file
    
    echo "Creating the submit page"

    cd submit/
    
    touch index.html
    mfheader index.html
    
    while read -l line
        echo $line >> index.html
    end < $file

    mffooter index.html

    cd ..
    
end
